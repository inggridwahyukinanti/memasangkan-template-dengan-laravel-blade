<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('adminlte.master'); //karena file master.blade.php berada di folder adminlte, jika master.blade.phpnya di folder views saja maka cukup ketik master saja
});

Route::get('/tableku', function () {
    return view('tableku.table');
});

Route::get('/tableku/tabeldatabase', function () {
    return view('tableku.tabeldatabase');
});